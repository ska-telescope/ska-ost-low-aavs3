# -*- coding: utf-8 -*-
"""This module contains pytest fixtures and hooks."""

import pytest


def pytest_sessionfinish(session, exitstatus):
    """
    Override the exit status in the case that no tests were run.

    This is a temporary workaround to cover the current situation
    in which there are no tests to run,
    but we still want the test step to pass.

    :param session: a pytest Session object
    :param exitstatus: the exit code of the pytest run
    """
    if exitstatus == pytest.ExitCode.NO_TESTS_COLLECTED:
        session.exitstatus = pytest.ExitCode.OK
