# ska-ost-low-aavs3

This repository contains observing and data analysis scripts
for the Aperture Array Verification System 3 (AAVS3).

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ost-low-aavs3/badge/?version=latest)](https://developer.skao.int/projects/ska-ost-low-aavs3/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-ost-low-aavs3 documentation](https://developer.skatelescope.org/projects/ska-ost-low-aavs3/en/latest/index.html "SKA Developer Portal: ska-ost-low-aavs3 documentation")
