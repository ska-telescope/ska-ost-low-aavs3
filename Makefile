include .make/base.mk

########################################
# DOCS
########################################

DOCS_SPHINXOPTS = -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs
