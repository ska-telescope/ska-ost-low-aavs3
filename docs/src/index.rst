ska-ost-low-aavs3
=================

The ``ska-ost-low-aavs3`` repository contains observing and data analysis scripts
for the Aperture Array Verification System 3 (AAVS3)
in the `Square Kilometre Array Observatory`_

Usage
^^^^^

Cloning
-------
Clone this repository with
```
git clone --recurse-submodules https://gitlab.com/ska-telescope/ska-ost-low-aavs3.git
```

Structure
---------
* Jupyter notebooks belong in the ``notebooks/`` folder.

* Pure python scripts belong in the ``scripts/`` folder.

Code quality
------------
The CI pipeline is set up to lint the contents of the above folders.
To ensure that the pipeline passes, you will want to lint your code locally.
This repository uses poetry to define an environment with the required dependencies.
To activate this environment:

1. Install poetry if you have not already done so: ``pip install poetry``.
2. Use poetry to install the required dependencies into a virtual environment: ``poetry install``.
3. Activate the virtual environment: ``poetry shell``.

You may now use the following commands to auto-format and lint your code:

* ``make notebook-format`` -- to auto-format your notebooks

* ``make notebook-lint`` -- to lint your notebooks

* ``make python-format`` -- to auto-format your python scripts

* ``make python-lint`` -- to lint your python scripts

.. _Square Kilometre Array Observatory: https://skao.int/

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
